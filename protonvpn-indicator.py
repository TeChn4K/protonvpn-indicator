#!/usr/bin/env python3
import subprocess
import os, sys, pwd
import signal
import threading
import re
import requests, json

import gi
gi.require_version('Gtk', '3.0')
gi.require_version('AppIndicator3', '0.1')

from gi.repository import Gtk, GLib, AppIndicator3

currpath = os.path.dirname(os.path.realpath(__file__))
INDICATOR_ID = 'protonvpn-applet'
iconpath = currpath + '/openvpn.svg'
iconConnectedpath = currpath + '/openvpn_lock.svg'

whoami = pwd.getpwuid(os.getuid())[0]

protonInfos = {
    'openvpn_status': False,
    'protonvpn_status': False,
    'internet_status': False,
    'ip': '',
    'server_name': '',
    'protocol': '',
    'country': '',
    'tier': '',
    'server_feature': '',
    'server_load': ''
}   

class Indicator: 
    def __init__(self):
        self.indicator = AppIndicator3.Indicator.new(INDICATOR_ID, iconpath, AppIndicator3.IndicatorCategory.SYSTEM_SERVICES)
        self.indicator.set_status(AppIndicator3.IndicatorStatus.ACTIVE)       
        self.indicator.set_menu(self.getMenu())

    def getMenu(self):
        menu = Gtk.Menu()
        countrySubMenu = Gtk.Menu()

        countryCode = getCountryCodes()
        for code in countryCode:
            menuItem = Gtk.MenuItem(label=code)
            menuItem.connect('activate', lambda _, code=code: connectCountry(code))
            countrySubMenu.append(menuItem)

        self.statusItem = Gtk.MenuItem(label='Initializing', sensitive=False)
        menu.append(self.statusItem)

        menuItem = Gtk.MenuItem(label='Refresh status')
        menuItem.connect('activate', lambda _: updateStatus())
        menu.append(menuItem)

        menu.append(Gtk.SeparatorMenuItem())

        self.fastItem = Gtk.MenuItem(label='Fast connect', sensitive=False)
        self.fastItem.connect('activate', lambda _: connectFast())
        menu.append(self.fastItem)

        self.p2pItem = Gtk.MenuItem(label='P2P connect', sensitive=False)
        self.p2pItem.connect('activate', lambda _: connectP2p())
        menu.append(self.p2pItem)

        self.countriesItem = Gtk.MenuItem(label='Countries', sensitive=False)
        self.countriesItem.set_submenu(countrySubMenu)
        menu.append(self.countriesItem)

        self.disconnItem = Gtk.MenuItem(label='Disconnect', sensitive=False)
        self.disconnItem.connect('activate', lambda _: disconnect())
        menu.append(self.disconnItem)

        menu.append(Gtk.SeparatorMenuItem())

        item_quit = Gtk.MenuItem(label='Quit')
        item_quit.connect('activate', stop)
        menu.append(item_quit)

        menu.show_all()
        return menu

    def setConnected(self):
        self.updateStatusText('Connected - ' + protonInfos['country'] + ' - ' + protonInfos['ip'])
        self.indicator.set_icon(iconConnectedpath)
        self.fastItem.set_sensitive(False)
        self.p2pItem.set_sensitive(False)
        self.countriesItem.set_sensitive(False)
        self.disconnItem.set_sensitive(True)

    def setConnecting(self):
        self.updateStatusText('Connecting ...')
        self.fastItem.set_sensitive(False)
        self.p2pItem.set_sensitive(False)
        self.countriesItem.set_sensitive(False)
        self.disconnItem.set_sensitive(True)

    def setDisconnected(self):
        self.updateStatusText('Disconnected')
        self.indicator.set_icon(iconpath)
        self.fastItem.set_sensitive(True)
        self.p2pItem.set_sensitive(True)
        self.countriesItem.set_sensitive(True)
        self.disconnItem.set_sensitive(False)

    def setDisconnecting(self):
        self.updateStatusText('Disconnecting ...')
        self.fastItem.set_sensitive(True)
        self.p2pItem.set_sensitive(True)
        self.countriesItem.set_sensitive(True)
        self.disconnItem.set_sensitive(False)


    def updateStatusText(self, value):
        self.statusItem.set_label(value)


def updateStatus():
    indicator.updateStatusText('Status updating ...')

    def callback(output):
        infos = extractProtonInfo(output)

        if (
            infos['openvpn_status'] == 1 and 
            infos['protonvpn_status'] == 1 and 
            infos['internet_status'] == 1
        ):
            GLib.idle_add(indicator.setConnected)
        else:
            GLib.idle_add(indicator.setDisconnected)

    cmd = ThreadedCmd(command='/usr/local/bin/protonvpn-cli -status', callback=callback)
    cmd.start()
    ThreadedCmd.runningThread.append(cmd)

def connectFast():
    indicator.setConnecting()

    ThreadedCmd.killAllThread()
    cmd = ThreadedCmd(command='/usr/local/bin/protonvpn-cli -f', callback=lambda _: updateStatus())
    cmd.start()
    ThreadedCmd.runningThread.append(cmd)

def connectP2p():
    indicator.setConnecting()

    ThreadedCmd.killAllThread()
    cmd = ThreadedCmd(command='/usr/local/bin/protonvpn-cli -p2p', callback=lambda _: updateStatus())
    cmd.start()
    ThreadedCmd.runningThread.append(cmd)

def connectCountry(code=None):
    if code:
        indicator.setConnecting()

        ThreadedCmd.killAllThread()
        cmd = ThreadedCmd(command='/usr/local/bin/protonvpn-cli -cc ' + code, callback=lambda _: updateStatus())
        cmd.start()
        ThreadedCmd.runningThread.append(cmd)

def disconnect():
    indicator.setDisconnecting()

    ThreadedCmd.killAllThread()
    cmd = ThreadedCmd(command='/usr/local/bin/protonvpn-cli -d', callback=lambda _: updateStatus())
    cmd.start()
    ThreadedCmd.runningThread.append(cmd)

def getCountryCodes():
    try:
        serversReq = requests.get("https://api.protonmail.ch/vpn/logicals")
    except requests.exceptions.RequestException as e:
        showErrorBox(e)

    if serversReq.status_code != requests.codes.ok:
        showErrorBox("Unable to download servers list ...")
    else:
        servers = json.loads(serversReq.text)['LogicalServers']
        
        countryCodes = []

        for server in servers:
            countryCodes.append(server['EntryCountry'])

        return list(sorted(set(countryCodes)))

def extractProtonInfo(text):
    m = re.search('\[OpenVPN Status\]: (.+)', text)
    protonInfos['openvpn_status'] = 1 if m and m.group(1) == 'Running' else 0

    m = re.search('\[ProtonVPN Status\]: (.+)', text)
    protonInfos['protonvpn_status'] = 1 if m and m.group(1) == 'Running' else 0

    m = re.search('\[Internet Status\]: (.+)', text)
    protonInfos['internet_status'] = 1 if m and m.group(1) == 'Online' else 0

    m = re.search('\[Public IP Address\]: (.+)', text)
    protonInfos['ip'] = m.group(1) if m else ''
    
    m = re.search('\[ProtonVPN\] \[Server Name\]: (.+)', text)
    protonInfos['server_name'] = m.group(1) if m else ''

    m = re.search('\[ProtonVPN\] \[OpenVPN Protocol\]: (.+)', text)
    protonInfos['protocol'] = m.group(1) if m else ''

    m = re.search('\[ProtonVPN\] \[Exit Country\]: (.+)', text)
    protonInfos['country'] = m.group(1) if m else ''

    m = re.search('\[ProtonVPN\] \[Tier\]: (.+)', text)
    protonInfos['tier'] = m.group(1) if m else ''

    m = re.search('\[ProtonVPN\] \[Server Features\]: (.+)', text)
    protonInfos['server_feature'] = m.group(1) if m else ''

    m = re.search('\[ProtonVPN\] \[Server Load\]: (.+)', text)
    protonInfos['server_load'] = m.group(1) if m else ''

    return protonInfos
    
def showErrorBox(msg): 
    dlg = Gtk.MessageDialog(type=Gtk.MessageType.ERROR, buttons=Gtk.ButtonsType.OK, message_format=msg)
    dlg.set_title('ProtonVPN Indicator - error')
    dlg.run()
    dlg.destroy()

def stop(source):
    Gtk.main_quit()


class ThreadedCmd( threading.Thread ):

    runningThread = []

    @classmethod
    def killAllThread(self):
        for thread in ThreadedCmd.runningThread:
            thread.stop()
            ThreadedCmd.runningThread.remove(thread)

    def __init__(self, command, callback=None):
        self.command = command
        self.callback = callback
        threading.Thread.__init__(self)
        self.daemon = True

    def run(self):

        self.process = subprocess.Popen(self.command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, preexec_fn=os.setsid)
                
        stdout, stderr = self.process.communicate()
        output = stdout.decode()

        if stderr:
            GLib.idle_add(showErrorBox, stderr)

        if self.callback is not None:
            self.callback(output)

    def stop(self):
        try:
            if self.process is not None:
                os.killpg(os.getpgid(self.process.pid), signal.SIGTERM)
        # Silently fail if process already exited or no exists anymore
        except (OSError, AttributeError) as e:
            pass

if __name__ == '__main__':
    if os.getuid() != 0:
        print(INDICATOR_ID + ' must run as root.')
        exit(1)

    indicator = Indicator()
    updateStatus()

    signal.signal(signal.SIGINT, signal.SIG_DFL)
    Gtk.main()