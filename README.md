# ProtonVPN Indicator

Display a ProtonVPN indicator for your GTK panel

This application is in early stages of development. There is no stable release yet.

This is not an official ProtonVPN application!

## Features

- ProtonVPN status in your notification tray
- Quick connect (fastest or P2P)
- Connect to a specific country (fastest)
- Show IP and country

## Requirements

- protonvpn-cli
- python3 & python-gi

## Install

- Install protonvpn-cli by following the official ProtonVPN documentation : https://protonvpn.com/support/linux-vpn-tool/

- Do not forget to initialize protonvpn-cli! `sudo protonvpn-cli -init`

_protonvpn-indicator is not able to detect if this step have correctly been done yet. It will fail if not!_

## Run

- `sudo python3 protonvpn-indicator.py`

You have to run this application as root... I tried some `pkexec` trails, but without any success for the moment.

## Todo

- Check requirements at startup
- Check protonvpn account type and filter allowed servers
- Do not require root to run the GUI
